import React from 'react'
import './Header.css'
const Header = () => {
    return (
        <div className='header'>
            <div className="header-contents">
                <h2>Order your favourite food here</h2>
                <p>I am going to show you a couple ways how you can change checkout button text. But please note, that this text can be changed dynamically depending on a payment method selected. We will also cover that below.
                </p>
                <button>View Menu</button>
            </div>
        </div>
    )
}

export default Header