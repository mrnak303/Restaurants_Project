import React, {useState} from 'react'
import Navbar from "./components/Navbar/Navbar.jsx";
import {Route, Routes} from "react-router-dom";
import Home from "./page/Home/Home.jsx";
import Cart from "./page/Cart/Cart.jsx";
import PlaceOrder from "./page/PlaceOrder/PlaceOrder.jsx";
import Footer from "./Footer/Footer.jsx";
import LoginPopup from "./components/LoginPopup/LoginPopup.jsx";

const App = () => {

    const [showLogin, setShowLogin] = useState(false)

    return (
        <>
            {showLogin ? <LoginPopup setShowLogin={setShowLogin}/> : <></>}
            <div className='app'>
                <Navbar setShowLogin={setShowLogin}/>
                <Routes>
                    < Route path='/' element={<Home/>}/>
                    <Route path='/cart' element={<Cart/>}/>
                    <Route path='/order' element={<PlaceOrder/>}/>
                </Routes>
            </div>
            <Footer/>
        </>
    )
}

export default App

